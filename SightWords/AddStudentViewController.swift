//
//  AddStudentViewController.swift
//  SightWords
//
//  Created by Lyndy Tankersley on 4/5/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

import UIKit
import Parse

class AddStudentViewController: UIViewController {
    
    @IBOutlet var age: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var firstName: UITextField!
    
    override func viewDidLoad() {
    

        // Do any additional setup after loading the view.
    }

    @IBAction func addStudent(_ sender: Any) {
        
        self.navigationController?.title = "Add Student"
        
        let teacher = PFUser.current()?.objectId
        let student = PFObject(className: "Students")
        student["teacher"] = teacher
        student["firstname"] = firstName.text
        student["lastname"] = lastName.text
        
        student["age"] = age.text
        student.saveInBackground{ (success, error) in
            
        }
        
        self.clearText()
        
    }
    
    
    func clearText(){
        firstName.text = ""
        lastName.text = ""
        age.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

struct MasterList {
    let masterArray = ["ball", "blue", "both", "even", "for", "help", "put", "there", "why", "follow", "yellow", "green", "she", "small", "see", "want", "over", "often", "grow", "should", "better", "cold", "idea", "over"]
}
