//
//  AddWordViewController.swift
//  SightWords
//
//  Created by Lyndy Tankersley on 4/15/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

import UIKit
import Parse

class AddWordViewController: UIViewController {
    @IBOutlet var addWordFld: UITextField!
    var studentId : String = ""
    var studentName : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func addWordAction(_ sender: Any) {
      
        if(addWordFld.text != ""){
            let word = PFObject(className: "Words")
            word["word"] = addWordFld.text
            word["passed"] = "n"
            word["tested"] = "n"
            word["studentId"] = self.studentId
            word["studentName"] = self.studentName
            word.saveInBackground()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
