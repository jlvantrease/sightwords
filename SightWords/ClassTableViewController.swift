//
//  ClassTableViewController.swift
//  SightWords
//
//  Created by Lyndy Tankersley on 4/5/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

import UIKit
import Parse

class ClassTableViewController: UITableViewController {
    var classArray = [Student]()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = PFUser.current()?.username
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier:"cell")
        
        let add = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addStudentBtnTapped))
        
        let logout = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logoutBtnTapped))
        
        navigationItem.rightBarButtonItems = [ logout, add]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        classArray.removeAll()
        
        let teacher = PFUser.current()?.objectId
        
        let students = PFQuery(className: "Students")
        
        students.whereKey("teacher", equalTo: teacher!)
        
        students.findObjectsInBackground{ (objects, error) in
            
            if(objects != nil){
                for obj in objects!{
                    let first = obj["firstname"] as! String
                    let last = obj["lastname"] as! String
                    let id = obj.objectId
                    let myStudent = Student(firstName: first, lastName: last, id: id!)
                    self.classArray.append(myStudent)
                    
                }
                self.tableView.reloadData()
            }
        }

    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classArray.count
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = classArray[indexPath.row].displayName

        return cell
    }
    
    func addStudentBtnTapped(){
        let asvc = AddStudentViewController(nibName: "AddStudentViewController", bundle: nil)
        self.navigationController?.pushViewController(asvc, animated: true)
    }
    
    
    func logoutBtnTapped(){
        PFUser.logOut()
        navigationController?.popViewController(animated: true)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let wtvc = WordTableViewController()
        
        wtvc.studentName = classArray[indexPath.row].displayName
        wtvc.studentId = classArray[indexPath.row].id
    
        self.navigationController?.pushViewController(wtvc, animated: true)
    }
}

struct Student {
    let firstName, lastName, displayName, id : String
    init(firstName : String, lastName : String, id : String) {
        self.firstName = firstName
        self.lastName = lastName
        self.id = id
        self.displayName = firstName + ", " + lastName
    }
}
