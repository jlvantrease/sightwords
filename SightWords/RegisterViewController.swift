//
//  RegisterViewController.swift
//  SightWords
//
//  Created by Lyndy Tankersley on 3/25/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

import UIKit
import Parse

class RegisterViewController: UIViewController {
    @IBOutlet var userNameTxtFld: UITextField!

    @IBOutlet var emailTxtFld: UITextField!
    
    @IBOutlet var passwordTxtFld: UITextField!
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Register"
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    @IBAction func registerBtnAction(_ sender: Any) {
        let user = PFUser()
        user.username = userNameTxtFld.text
        user.email = emailTxtFld.text
        user.password = passwordTxtFld.text
        user.signUpInBackground()
        navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
