//
//  ViewController.swift
//  SightWords
//
//  Created by Lyndy Tankersley on 3/23/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController {

    @IBOutlet var userNametTxtFld: UITextField!
    @IBOutlet var passwordTxtFld: UITextField!
    
    @IBAction func loginBtnAction(_ sender: Any) {
        
        let username = userNametTxtFld.text
        let password = passwordTxtFld.text
        
        PFUser.logInWithUsername(inBackground: username! , password: password! ){ (user, error) in
            
            if((user) != nil){
                
                let crvc = ClassTableViewController(nibName: "ClassTableViewController", bundle: nil)
                
                self.navigationController?.pushViewController(crvc, animated: true)

                
            }else{
                let alert = UIAlertController(title: "Login Error", message: "Invalid Login", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    @IBAction func reigsterBtnAction(_ sender: Any) {
        let rvc = RegisterViewController(nibName: "RegisterViewController", bundle: nil)
        navigationController?.pushViewController(rvc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //PFUser.logOut()
      
        if(PFUser.current() != nil){
            loadStudentsList()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func loadStudentsList(){
        
        let crvc = ClassTableViewController(nibName: "ClassTableViewController", bundle: nil)
        
        self.navigationController?.pushViewController(crvc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

