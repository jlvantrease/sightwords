//
//  WordTableViewController.swift
//  SightWords
//
//  Created by Lyndy Tankersley on 4/5/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

import UIKit
import Parse


class WordTableViewController: UITableViewController {
    
    var studentName = ""
    var studentId = ""
    var sightWordsArray = [Word]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = studentName
        
        //tableView.register(UITableViewCell.self, forCellReuseIdentifier:"cell")
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addWordBtnTapped))
        
        let checkWords = PFQuery(className: "Words")
        checkWords.whereKey("studentId", equalTo: studentId)
        checkWords.findObjectsInBackground{ (objects, error) in
        
            if((objects?.count)! <= 0){
                let myArray = MasterList()
        
                var pfArray = [PFObject]()
        
                for element in myArray.masterArray{
                    let word = PFObject(className: "Words")
                    word["word"] = element
                    word["passed"] = "n"
                    word["tested"] = "n"
                    word["studentId"] = self.studentId
                    word["studentName"] = self.studentName
                    pfArray.append(word)
                }
        
                PFObject.saveAll(inBackground: pfArray)
            }
        }
        
        /*
        let getWords = PFQuery(className: "Words")
        getWords.whereKey("studentId", equalTo: studentId)
        getWords.whereKey("passed", equalTo: "n")
        getWords.findObjectsInBackground{ (objects, error) in
             
            if(objects != nil){
                for obj in objects!{
                    let word = obj["word"] as! String
                    let passed = obj["passed"] as! String
                    let tested = obj["tested"] as! String
                    let studentId = obj["studentId"] as! String
                    let id = obj.objectId
                    let myWord = Word(word: word, passed: passed, tested: tested, studentId: studentId, id: id!)
                    self.sightWordsArray.append(myWord)
                }
            self.tableView.reloadData()
            }
        self.tableView.reloadData()
        }
        */
        tableView.register(UITableViewCell.self, forCellReuseIdentifier:"cell")
        
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        sightWordsArray.removeAll()
        let getWords = PFQuery(className: "Words")
        getWords.whereKey("studentId", equalTo: studentId)
        getWords.whereKey("passed", equalTo: "n")
        getWords.findObjectsInBackground{ (objects, error) in
            
            if(objects != nil){
                for obj in objects!{
                    let word = obj["word"] as! String
                    let passed = obj["passed"] as! String
                    let tested = obj["tested"] as! String
                    let studentId = obj["studentId"] as! String
                    let id = obj.objectId
                    let myWord = Word(word: word, passed: passed, tested: tested, studentId: studentId, id: id!)
                    self.sightWordsArray.append(myWord)
                }
                self.tableView.reloadData()
            }
            self.tableView.reloadData()
        }
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier:"cell")
        
        tableView.reloadData()
    
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sightWordsArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        cell.textLabel?.text = sightWordsArray[indexPath.row].word
            //sightWordsArray[indexPath.row]
        return cell
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    func addWordBtnTapped(){
        let awvc = AddWordViewController(nibName: "AddWordViewController", bundle: nil)
        awvc.studentId = self.studentId
        awvc.studentName = self.studentName
        self.navigationController?.pushViewController(awvc, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let right = UITableViewRowAction(style: .normal, title: "Right"){action, index in
            
            let query = PFQuery(className: "Words")
            
            query.getObjectInBackground(withId: self.sightWordsArray[indexPath.row].id){ (objects, error) in
                
                if(objects != nil){
                    objects?.setValue("y", forKey: "passed")
                    objects?.setValue("y", forKey: "tested")
                    objects?.saveInBackground()
                }
            }
            
            self.sightWordsArray.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
            
            print ("right")
        }
        right.backgroundColor = UIColor.green
        
        let wrong = UITableViewRowAction(style: .normal, title: "Wrong"){action, index in
           
            self.sightWordsArray.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
            print ("wrong")
         
        }
        wrong.backgroundColor = UIColor.red
        return [wrong, right]
    }

}
struct Word {
    let word, passed, tested, studentId, id : String
    
    init(word : String, passed : String, tested: String, studentId :String, id : String) {
        self.word = word
        self.passed = passed
        self.tested = tested
        self.studentId = id
        self.id = id
    }
}
